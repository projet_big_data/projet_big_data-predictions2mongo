# projet_big_data-predictions2mongo
Projet permettant d'ajouter les données de prédictions à la base MongoDB

## Utilisation

Le script n'cessite que l'on connaise l'adresse de la base MongoDB et le nom de la database+collection, il ne prend pas en charge les bases avec sécurité (user/motdepasse).
### Parametration

Les variables à modifié sont dans le fichier **env.config**

La variable *databaseName* nom de la database de MongoDB
La variable *datafolder* correspond au dossier contenant les fichiers de prédictions
La variable *testCL* nom de la collection de MongoDB

### Lancement 

Utilisez la commande
```
./ import2mongo.sh
```

