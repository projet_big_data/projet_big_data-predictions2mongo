#!/bin/bash
. env.config


# Importation des documents du fichier 
listfiles=$(ls $datafolder)
for file in $listfiles
do

text=$(tr -d "\n\r" < $datafolder$file)
textCleaned=$(echo $text | tr -d " " )

mongo 127.0.0.1/$databaseName --eval "var document = $textCleaned; db.testCL.insert(document);"

done


